package com.reidzeibel.qasirtestapp.base

interface BasePresenter<in T: BaseView> {

    fun subscribe()

    fun unsubscribe()

    fun onResume()

    fun onDestroy()

    fun setTargetView(targetView: T)

}