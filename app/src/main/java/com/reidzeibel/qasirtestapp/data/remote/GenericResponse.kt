package com.reidzeibel.qasirtestapp.data.remote

data class GenericResponse<T> (
        val status: String,
        val message: String,
        val data: T
)