package com.reidzeibel.qasirtestapp.data.remote.product

import io.reactivex.Single

interface ProductManager {

    fun getAllProducts() : Single<ProductData>

    fun getProduct(productId: Long) : Single<Product>

}