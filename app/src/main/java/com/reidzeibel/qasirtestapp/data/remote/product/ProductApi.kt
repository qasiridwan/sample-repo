package com.reidzeibel.qasirtestapp.data.remote.product

import com.reidzeibel.qasirtestapp.data.remote.Endpoints
import com.reidzeibel.qasirtestapp.data.remote.GenericResponse
import io.reactivex.Single
import retrofit2.http.GET

interface ProductApi {

    @GET(Endpoints.PRODUCTS)
    fun getAllProducts() : Single<GenericResponse<ProductData>>

}