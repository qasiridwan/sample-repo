package com.reidzeibel.qasirtestapp.data.remote.product

import android.content.res.Resources
import com.google.gson.annotations.SerializedName

data class ProductData(
        val banner: Banner,
        val products: List<Product>
)

data class Banner(val image: String)

data class Images(
    val thumbnail: String,
    val large: String
)

data class Product(
    @SerializedName("product_id") val productId : Long,
    @SerializedName("product_name") val productName : String,
    val price: Long,
    val stock: Int,
    val description: String,
    val images: Images
)

fun List<Product>.contains(id: Long): Boolean {
    this.map { if (it.productId == id) return true }
    return false
}

fun List<Product>.get(id: Long): Product {
    this.map { if (it.productId == id) return it }
    throw Resources.NotFoundException()
}
