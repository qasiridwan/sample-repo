package com.reidzeibel.qasirtestapp.data.exceptions

class ProductNotFoundException(id: Long) :
    GenericMessageException(
        "Product with id $id not found, please try reopening the app"
    )