package com.reidzeibel.qasirtestapp.utils

import java.text.NumberFormat
import java.util.*

class CurrencyUtil {

    companion object {
        @JvmStatic
        fun formatCurrency(price: Any): String {
            return NumberFormat.getCurrencyInstance(Locale("in", "ID")).format(price)
                .replace("(?<=[A-Za-z])(?=[0-9])|(?<=[0-9])(?=[A-Za-z])".toRegex(), ".").replace(",00", "")
        }
    }
}