package com.reidzeibel.qasirtestapp.views.adapters

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.reidzeibel.qasirtestapp.R

class ProductItemDecorator : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        if (parent.getChildAdapterPosition(view) == (parent.adapter!!.itemCount - 1)) {
            outRect.bottom = view.resources.getDimensionPixelSize(R.dimen.recycler_margin_top_bottom)
        }

        if (parent.getChildAdapterPosition(view) == 0) {
            outRect.top = view.resources.getDimensionPixelSize(R.dimen.recycler_margin_top_bottom)
        }
    }
}