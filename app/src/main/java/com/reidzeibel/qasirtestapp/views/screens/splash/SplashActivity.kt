package com.reidzeibel.qasirtestapp.views.screens.splash

import com.reidzeibel.qasirtestapp.R
import com.reidzeibel.qasirtestapp.base.BaseActivity
import com.reidzeibel.qasirtestapp.di.components.DaggerSplashComponent
import com.reidzeibel.qasirtestapp.di.components.SplashComponent
import com.reidzeibel.qasirtestapp.di.modules.SplashModule
import javax.inject.Inject

class SplashActivity : BaseActivity<SplashComponent>(), SplashContract.View {

    @Inject
    lateinit var presenter: SplashContract.Presenter

    override fun getLayout(): Int = R.layout.activity_splash

    override fun initInjector(): SplashComponent =
        DaggerSplashComponent.builder()
            .applicationComponent(getApp().appComponent)
            .splashModule(SplashModule())
            .build()

    override fun inject(component: SplashComponent) = component.inject(this)

    override fun initViews() {
        addPresenter(presenter, this)
    }

    override fun onProductLoadSuccess() {
        navigator.navigateToHomeScreen(this)
        finish()
    }

    override fun onProductLoadFailed(throwable: Throwable) {
        showInAppError("Failed fetching product data", throwable.message!!)
    }
}
