package com.reidzeibel.qasirtestapp.views.screens

import android.content.Context
import android.content.Intent
import androidx.fragment.app.Fragment
import com.reidzeibel.qasirtestapp.views.screens.home.HomeActivity
import com.reidzeibel.qasirtestapp.views.screens.home.productdetail.ProductDetailFragment
import com.reidzeibel.qasirtestapp.views.screens.home.productlist.ProductListFragment
import timber.log.Timber
import javax.inject.Inject

class Navigator @Inject constructor() {

    fun navigateToHomeScreen(context: Context) {
        Timber.tag(TAG).d("Navigate to Home Screen")
        context.startActivity(Intent(context, HomeActivity::class.java))
    }

    fun getProductListFragment(context: Context): Fragment {
        Timber.tag(TAG).i("Attached: ProductListFragment to ${context::class.java.simpleName}")
        return ProductListFragment.newInstance()
    }

    fun getProductDetailFragment(context: Context, productId: Long): Fragment {
        Timber.tag(TAG).i("Attached: ProductDetailFragment to ${context::class.java.simpleName}")
        return ProductDetailFragment.newInstance(productId)
    }

    companion object {
        const val TAG = "Navigator"
    }
}