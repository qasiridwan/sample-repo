package com.reidzeibel.qasirtestapp.views.screens.home.productlist

import com.reidzeibel.qasirtestapp.base.BasePresenter
import com.reidzeibel.qasirtestapp.base.BaseView
import com.reidzeibel.qasirtestapp.data.remote.product.ProductData

interface ProductListContract {

    interface View: BaseView {

        fun onGetProductSuccess(productData: ProductData)

        fun onGetProductFailed(error: String)

    }

    interface Presenter: BasePresenter<View>

}