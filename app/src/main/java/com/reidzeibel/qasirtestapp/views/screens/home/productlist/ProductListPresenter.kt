package com.reidzeibel.qasirtestapp.views.screens.home.productlist

import com.reidzeibel.qasirtestapp.base.BaseRxPresenter
import com.reidzeibel.qasirtestapp.data.remote.product.ProductManager
import javax.inject.Inject

class ProductListPresenter @Inject constructor(
    private val productManager: ProductManager
) : BaseRxPresenter(), ProductListContract.Presenter {

    lateinit var view: ProductListContract.View

    override fun setTargetView(targetView: ProductListContract.View) {
        view = targetView
    }

    override fun onResume() {
        manage(
            productManager.getAllProducts()
                .subscribe(
                    { view.onGetProductSuccess(it) },
                    { throwable -> throwable.message?.let { view.onGetProductFailed(it) }}
                )
        )
    }
}