package com.reidzeibel.qasirtestapp.views.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.reidzeibel.qasirtestapp.R
import com.reidzeibel.qasirtestapp.data.remote.product.Product
import com.reidzeibel.qasirtestapp.views.adapters.viewholders.ProductViewHolder
import io.reactivex.subjects.PublishSubject
import java.util.*
import javax.inject.Inject

class ProductListAdapter @Inject constructor() : RecyclerView.Adapter<ProductViewHolder>() {

    internal val singleClickPublish = PublishSubject.create<Product>()
    private val mProductList: ArrayList<Product> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_product, parent, false)
        return ProductViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        holder.bind(mProductList[position])
        holder.itemView.setOnClickListener {
            singleClickPublish.onNext(holder.boundItem)
        }
    }

    fun setProducts(products: List<Product>) {
        mProductList.clear()
        mProductList.addAll(products)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return mProductList.size
    }
}