package com.reidzeibel.qasirtestapp.views.screens.home.productdetail

import com.reidzeibel.qasirtestapp.base.BaseRxPresenter
import com.reidzeibel.qasirtestapp.data.remote.product.ProductManager
import javax.inject.Inject

class ProductDetailPresenter @Inject constructor(
    private val productManager: ProductManager
) : BaseRxPresenter(), ProductDetailContract.Presenter {

    lateinit var view: ProductDetailContract.View

    override fun setTargetView(targetView: ProductDetailContract.View) {
        view = targetView
    }

    override fun getProduct(productId: Long) {
        manage(
            productManager.getProduct(productId)
                .subscribe(
                    { view.onGetProductSuccess(it) },
                    { throwable -> throwable.message?.let { view.onGetProductFailed(it) }}
                )
        )
    }

}