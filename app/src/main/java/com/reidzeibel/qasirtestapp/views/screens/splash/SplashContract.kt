package com.reidzeibel.qasirtestapp.views.screens.splash

import com.reidzeibel.qasirtestapp.base.BasePresenter
import com.reidzeibel.qasirtestapp.base.BaseView

interface SplashContract {

    interface View: BaseView {

        fun onProductLoadSuccess()

        fun onProductLoadFailed(throwable: Throwable)

    }

    interface Presenter: BasePresenter<View>

}