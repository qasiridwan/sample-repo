package com.reidzeibel.qasirtestapp.views.screens.splash

import com.reidzeibel.qasirtestapp.base.BaseRxPresenter
import com.reidzeibel.qasirtestapp.data.remote.product.ProductManager
import io.reactivex.android.schedulers.AndroidSchedulers
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class SplashPresenter @Inject constructor(
    private val productManager: ProductManager
) : BaseRxPresenter(), SplashContract.Presenter {

    lateinit var view : SplashContract.View

    override fun setTargetView(targetView: SplashContract.View) {
        view = targetView
    }

    override fun subscribe() {
        manage(
            productManager.getAllProducts()
                .delay(1200, TimeUnit.MILLISECONDS, AndroidSchedulers.mainThread())
                .subscribe (
                    { view.onProductLoadSuccess() },
                    { view.onProductLoadFailed(it) }
                )
        )
    }
}