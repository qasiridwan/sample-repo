package com.reidzeibel.qasirtestapp.di.components

import com.reidzeibel.qasirtestapp.di.PerFragment
import com.reidzeibel.qasirtestapp.di.modules.ProductModule
import com.reidzeibel.qasirtestapp.views.screens.home.productdetail.ProductDetailFragment
import com.reidzeibel.qasirtestapp.views.screens.home.productlist.ProductListFragment
import dagger.Component

@PerFragment
@Component(dependencies = [ApplicationComponent::class], modules = [ProductModule::class])
interface ProductComponent {

    fun inject(productListFragment: ProductListFragment)

    fun inject(productDetailFragment: ProductDetailFragment)

}