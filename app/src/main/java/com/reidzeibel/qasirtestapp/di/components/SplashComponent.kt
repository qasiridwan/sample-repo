package com.reidzeibel.qasirtestapp.di.components

import com.reidzeibel.qasirtestapp.di.PerActivity
import com.reidzeibel.qasirtestapp.di.modules.SplashModule
import com.reidzeibel.qasirtestapp.views.screens.splash.SplashActivity
import dagger.Component

@PerActivity
@Component(dependencies = [ApplicationComponent::class], modules = [SplashModule::class])
interface SplashComponent {

    fun inject(splashActivity: SplashActivity)

}