package com.reidzeibel.qasirtestapp.di.components

import com.reidzeibel.qasirtestapp.di.PerActivity
import com.reidzeibel.qasirtestapp.views.screens.home.HomeActivity
import dagger.Component

@PerActivity
@Component
interface HomeComponent {

    fun inject(homeActivity: HomeActivity)

}